<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S01: PHP Basics and Selection Control</title>
</head>

<body>
    <!-- <h1>Hello World</h1> -->
    <h1>Echoing Values</h1>

    <p>
        <?php
        echo 'Good day $name ! Your given email is $email.';
        ?>
    </p>
    <p>
        <?php
        echo 'Good day ' . $name . ' ! Your given email is ' . $email . ".";
        ?>
    </p>
    <p>
        <?php
        echo "Good day $name ! Your given email is $email.";
        ?>
    </p>
    <p>
        <?php
        echo PI;
        ?>
    </p>

    <p> <?php echo $headCount; ?> </p>
    <h2> Data Types </h2>

    <h3> Strings </h3>
    <p> <?php echo $address; ?> </p>

    <h3> Integers / Float </h3>
    <p> <?php echo $age; ?> </p>
    <p> <?php echo $grade; ?> </p>

    <h3> Boolean / Null </h3>
    <p> <?php echo " hasTravelledAborad: $hasTravelledAbroad"; ?> </p>
    <p> <?php echo "spouse: $spouse"; ?> </p>

    <!-- gettype() or var_dump -->
    <p> <?php echo gettype($hasTravelledAbroad); ?> </p>
    <p> <?php echo gettype($spouse); ?> </p>

    <!-- 
        var_dump function dumps information about one or more variables
     -->
    <h3>Using var_dump</h3>
    <p> <?php echo var_dump($hasTravelledAbroad); ?> </p>
    <p> <?php echo var_dump($spouse); ?> </p>

    <h3>Arrays</h3>
    <p> <?php var_dump($grades); ?> </p>
    <p> <?php print_r($grades); ?> </p>
    <p> <?php echo $grades[3]; ?> </p>

    <h3>Objects</h3>
    <p> <?php echo var_dump($personObj); ?> </p>
    <p> <?php print_r($personObj); ?> </p>
    <p> <?php echo $gradesObj->firstGrading; ?> </p>
    <p> <?php echo $personObj->address->state; ?> </p>

    <h2>Operators</h2>

    <p>X: <?php echo $x; ?> </p>
    <p>Y: <?php echo $y; ?> </p>


    <p>Is Legal Age: <?php echo var_dump($isLegalAge); ?> </p>
    <p>Is Registered: <?php echo var_dump($isRegistered); ?> </p>

    <h2>Arithmetic Operators</h2>
    <p>Sum: <?php echo $x + $y; ?> </p>
    <p>Difference: <?php echo $x - $y; ?> </p>
    <p>Product: <?php echo $x * $y; ?> </p>
    <p>Quotient: <?php echo $x / $y; ?> </p>
    <p>Modulo: <?php echo $x % $y; ?> </p>

    <h2>Equality Operators</h2>
    <p>Loose Equality <?php echo var_dump($x == '500'); ?></p>
    <p>Strict Equality <?php echo var_dump($x === '500'); ?></p>
    <p>Loose Inequality <?php echo var_dump($x != '500'); ?></p>
    <p>Strict Inequality <?php echo var_dump($x !== '500'); ?></p>
    <p> <?php ?> </p>

    <h2>Greater/Lesser Operators</h2>
    <p>Is Lesser: <?php echo var_dump($x < $y); ?> </p>
    <p>Is Greater: <?php echo var_dump($x > $y); ?> </p>

    <p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?> </p>
    <p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?> </p>

    <h2>Logical Operators</h2>
    <p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered) ?> </p>
    <p>Are Some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered) ?> </p>

    <p>Are All Requirements Not Met: <?php echo var_dump(!$isLegalAge && !$isRegistered) ?> </p>
    <p>Are Some Requirements Not Met: <?php echo var_dump(!$isLegalAge || !$isRegistered) ?> </p>


    <h2>Function :</h2>
    <p>Fullname: <?php echo getFullName('John', 'D', 'Smith') ?> </p>

    <h2>Selection Control Structures</h2>
    <h3>If-ElseIf-Else</h3>
    <p> <?php echo determineTyphoonIntensity(12); ?></p>

    <h2> Ternary Sample (is Underage?) </h2>
    <p>25: <?php var_dump(isUnderAge(25)); ?> </p>
    <p>16: <?php var_dump(isUnderAge(16)); ?> </p>

    <h2> Switch Statement </h2>
    <p> <?php echo determineComputerUser(4) ?> </p>

    <h2>Try-Catch-Finally</h2>

    <p> <?php echo greeting("Hello"); ?> </p>



</body>

</html>